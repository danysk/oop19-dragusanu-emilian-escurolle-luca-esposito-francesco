import de.aaschmid.gradle.plugins.cpd.Cpd

plugins {
    java
    checkstyle
    pmd
    application
    id("com.github.johnrengelman.shadow") version "5.2.0"
    id("de.aaschmid.cpd") version "3.1"
    id("com.github.spotbugs") version "4.3.0"
    `build-dashboard`
}

sourceSets {
    main {
        java {
            srcDirs("src")
        }
        resources {
            srcDirs("res")
        }
    }
    test {
        java {
            setSrcDirs(setOf(file("src")))
        }
    }
}

val libraryFolder = "${project.projectDir}/lib"
repositories {
    mavenCentral()
    flatDir {
        dirs(libraryFolder)
    }
}

dependencies {
	implementation("junit:junit:_")
    implementation("org.junit.jupiter:junit-jupiter-api:_")
    runtimeOnly("org.junit.jupiter:junit-jupiter-engine:_")
    runtimeOnly("org.junit.vintage:junit-vintage-engine:_")
    File(libraryFolder)
        .takeIf { it.exists() }
        ?.takeIf { it.isDirectory }
        ?.listFiles()
        ?.filter { it.extension == "jar" }
        ?.map { it.absolutePath }
        ?.also{ println(it) }
        ?.let { implementation(files(it)) }
}

application {
    mainClassName = "it.unibo.oop.bounce.main.Main"
}

tasks.withType<Test> {
    useJUnitPlatform()
    systemProperty("java.library.path", "$libraryFolder${File.pathSeparator}${System.getProperty("java.library.path")}")
    systemProperty("org.lwjgl.librarypath", "$libraryFolder${File.pathSeparator}${System.getProperty("java.library.path")}")
}

spotbugs {
    setEffort("max")
    setReportLevel("low")
    showProgress.set(true)
    val excludeFile = File("${project.rootProject.projectDir}/config/spotbugs/excludes.xml")
    if (excludeFile.exists()) {
        excludeFilter.set(excludeFile)
    }
}

tasks.withType<com.github.spotbugs.snom.SpotBugsTask> {
    ignoreFailures = true
    reports {
        create("html") {
            enabled = true
        }
    }
}

pmd {
    ruleSets = listOf()
    ruleSetConfig = resources.text.fromFile("${project.rootProject.projectDir}/config/pmd/pmd.xml")
    isIgnoreFailures = true
}

cpd {
    isIgnoreFailures = true
}

tasks.withType<Cpd> {
    reports {
        xml.setEnabled(false)
        text.setEnabled(true)
    }
    language = "java"
    minimumTokenCount = 50
    ignoreFailures = true
    source = sourceSets["main"].allJava
}

checkstyle {
    isIgnoreFailures = true
}
