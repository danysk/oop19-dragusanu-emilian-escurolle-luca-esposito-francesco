Bounce

Il gruppo si pone l’obbiettivo di realizzare il gioco “Bounce”, uscito nel 2001 per Nokia.
Nel gioco il player controlla una pallina attraverso vari livelli 2D a scorrimento orizzontale.
In questi stage il giocatore deve evitare di scontrarsi con i vari ostacoli presenti sulla mappa e raggiungere il traguardo, così da accedere al prossimo livello. 
Il tutto risulta un'avventura dalla difficoltà incrementale.

Il file .jar è scaricabile nella sezione Downloads(https://bitbucket.org/Dragu99/oop19-bounce-emilian-dragusanu-luca-escurolle-francesco/downloads/)

La relazione è presente nella cartella Relazione.


